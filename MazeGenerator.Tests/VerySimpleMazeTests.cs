﻿using System.Collections.Generic;
using FluentAssertions;
using MazeGenerator.Factories;
using MazeGenerator.MazeBuilders;
using MazeGenerator.Mazes;
using MazeGenerator.Rooms;
using MazeGenerator.Traps;
using MazeGenerator.Types;
using MazeGenerator.Util;
using Moq;
using NUnit.Framework;

namespace MazeGenerator.Tests
{
    [TestFixture]
    public class VerySimpleMazeTests
    {
        private Mock<RandomGenerator> _randomMock;
        private Mock<RoomFactory> _roomFactoryMock;
        private Mock<TrapFactory> _trapFactoryMock;

        [SetUp]
        public void Setup()
        {
            _randomMock = new Mock<RandomGenerator>();
            SetupRandomGeneratorMock();

            _roomFactoryMock = new Mock<RoomFactory>();
            SetupRoomFactoryMock();

            _trapFactoryMock = new Mock<TrapFactory>(_randomMock.Object);
            SetupTrapFactoryMock();
        }

        private void SetupTrapFactoryMock()
        {
            _trapFactoryMock
                .Setup(mock => mock.CreateTrap(It.Is<TrapType>(x => x == TrapType.Marsh)))
                .Returns<TrapType>(type => new MarshTrap(_randomMock.Object));
        }

        private void SetupRoomFactoryMock()
        {
            _roomFactoryMock
                .Setup(mock => mock.CreateRoom(It.Is<RoomType>(x => x == RoomType.Marsh), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<List<Trap>>(), It.IsAny<Dictionary<Direction, int>>()))
                .Returns<RoomType, int, bool, List<Trap>, Dictionary<Direction, int>>((type, id, hasTreasure, traps, siblings) => new MarshRoom(id, hasTreasure, traps, siblings));
        }

        private void SetupRandomGeneratorMock()
        {
            _randomMock
                .Setup(mock => mock.GetEnumValue<RoomType>())
                .Returns(RoomType.Marsh);
            _randomMock
                .Setup(mock => mock.GetNumber(It.IsAny<int>(), It.IsAny<int>()))
                .Returns<int, int>((min, max) => (min + max) / 2);
            _randomMock
                .Setup(mock => mock.GetProbabilityValue())
                .Returns(0.25);
        }

        [TestCase(3)]
        [TestCase(30)]
        public void VerySimpleMaze_TestFullTraversal(int size)
        {
            var builder = new VerySimpleMazeBuilder(_roomFactoryMock.Object, _trapFactoryMock.Object, _randomMock.Object);

            var maze = builder.Build(size);
            var entranceRoom = maze.GetRoomById(maze.EntranceRoomId);

            var markedRoomIds = new List<int>();
            TestRoom(entranceRoom, maze, markedRoomIds);

            markedRoomIds.Count.Should().Be(size * size);
        }

        private void TestRoom(Room room, Maze maze, List<int> markedRooms)
        {
            markedRooms.Add(room.Id);
            TestDirection(room, Direction.North, maze, markedRooms);
            TestDirection(room, Direction.East, maze, markedRooms);
            TestDirection(room, Direction.South, maze, markedRooms);
            TestDirection(room, Direction.West, maze, markedRooms);
        }

        private void TestDirection(Room room, Direction direction, Maze maze, List<int> markedRooms)
        {
            var nextRoomId = room.GetAdjacentRoomId(direction);
            if (nextRoomId != null && !markedRooms.Contains(nextRoomId.Value))
            {
                var nextRoom = maze.GetRoomById(nextRoomId.Value);
                TestRoom(nextRoom, maze, markedRooms);
            }
        }
    }
}
