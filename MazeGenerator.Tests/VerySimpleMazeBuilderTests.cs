using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using MazeGenerator.Factories;
using MazeGenerator.MazeBuilders;
using MazeGenerator.Rooms;
using MazeGenerator.Traps;
using MazeGenerator.Types;
using MazeGenerator.Util;
using NUnit.Framework;
using Moq;

namespace MazeGenerator.Tests
{
    [TestFixture]
    public class VerySimpleMazeBuilderTests
    {
        private Mock<RandomGenerator> _randomMock;
        private Mock<RoomFactory> _roomFactoryMock;
        private Mock<TrapFactory> _trapFactoryMock;

        [SetUp]
        public void Setup()
        {
            _randomMock = new Mock<RandomGenerator>();
            SetupRandomGeneratorMock();

            _roomFactoryMock = new Mock<RoomFactory>();
            SetupRoomFactoryMock();

            _trapFactoryMock = new Mock<TrapFactory>(_randomMock.Object);
            SetupTrapFactoryMock();
        }

        private void SetupTrapFactoryMock()
        {
            _trapFactoryMock
                .Setup(mock => mock.CreateTrap(It.Is<TrapType>(x => x == TrapType.Marsh)))
                .Returns<TrapType>(type => new MarshTrap(_randomMock.Object));
        }

        private void SetupRoomFactoryMock()
        {
            _roomFactoryMock
                .Setup(mock => mock.CreateRoom(It.Is<RoomType>(x => x == RoomType.Marsh), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<List<Trap>>(), It.IsAny<Dictionary<Direction, int>>()))
                .Returns<RoomType, int, bool, List<Trap>, Dictionary<Direction, int>>((type, id, hasTreasure, traps, siblings) => new MarshRoom(id, hasTreasure, traps, siblings));
        }

        private void SetupRandomGeneratorMock()
        {
            _randomMock
                .Setup(mock => mock.GetEnumValue<RoomType>())
                .Returns(RoomType.Marsh);
            _randomMock
                .Setup(mock => mock.GetNumber(It.IsAny<int>(), It.IsAny<int>()))
                .Returns<int, int>((min, max) => (min + max) / 2);
            _randomMock
                .Setup(mock => mock.GetProbabilityValue())
                .Returns(0.25);
        }

        [Test]
        public void Build_MarshMazeOfSizeThree_ShouldBeValid()
        {
            var builder = new VerySimpleMazeBuilder(_roomFactoryMock.Object, _trapFactoryMock.Object, _randomMock.Object);

            var maze = builder.Build(3);

            maze.Rooms.Count.Should().Be(9);
            maze.Rooms.Select(x => x.Id).Distinct().Count().Should().Be(9);
            maze.Rooms.Count(x => x.HasTreasure).Should().Be(1);
            maze.Rooms.Count(x => x.Id == maze.EntranceRoomId).Should().Be(1);
            foreach (var room in maze.Rooms)
            {
                room.Should().BeOfType<MarshRoom>();
            }
        }

        [Test]
        public void Build_WhenInvalidSize_ShouldFail()
        {
            var builder = new VerySimpleMazeBuilder(_roomFactoryMock.Object, _trapFactoryMock.Object, _randomMock.Object);

            Action act = () => builder.Build(1);
            act.Should().Throw<ArgumentOutOfRangeException>();
        }
    }
}