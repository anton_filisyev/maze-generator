﻿using System.Collections.Generic;
using FluentAssertions;
using MazeGenerator.Rooms;
using MazeGenerator.Traps;
using MazeGenerator.Types;
using MazeGenerator.Util;
using Moq;
using NUnit.Framework;

namespace MazeGenerator.Tests
{
    [TestFixture]
    public class RoomTests
    {
        [TestCase(true)]
        [TestCase(false)]
        public void HasTreasure_ShouldBeSet(bool hasTreasure)
        {
            var room = new MarshRoom(1, hasTreasure, new List<Trap>(), new Dictionary<Direction, int>());

            room.HasTreasure.Should().Be(hasTreasure);
        }

        [Test]
        public void GetAdjacentRoom_WhenDefault_ShouldBeEmpty()
        {
            var room = new MarshRoom(1, false, new List<Trap>(), new Dictionary<Direction, int>());

            room.GetAdjacentRoomId(Direction.North).Should().BeNull();
            room.GetAdjacentRoomId(Direction.South).Should().BeNull();
            room.GetAdjacentRoomId(Direction.East).Should().BeNull();
            room.GetAdjacentRoomId(Direction.West).Should().BeNull();
        }

        public void GetAdjacentRoom_WhenSetEast_ShouldBeSingle()
        {
            var anotherRoom = new MarshRoom(2, false, new List<Trap>(), new Dictionary<Direction, int>());

            var adjacentRoomIds = new Dictionary<Direction, int>()
            {
                {Direction.East, anotherRoom.Id}
            };
            var room = new MarshRoom(1, false, new List<Trap>(), adjacentRoomIds);

            room.GetAdjacentRoomId(Direction.North).Should().BeNull();
            room.GetAdjacentRoomId(Direction.South).Should().BeNull();
            room.GetAdjacentRoomId(Direction.West).Should().BeNull();
            room.GetAdjacentRoomId(Direction.East).Should().NotBeNull();
            room.GetAdjacentRoomId(Direction.East).Should().Be(anotherRoom.Id);
        }

        [Test]
        public void CausesInjury_WhenNoTrap_ShouldBeFalse()
        {
            var room = new MarshRoom(1, false, new List<Trap>(), new Dictionary<Direction, int>());

            var trapActivated = room.CausesInjury();

            trapActivated.Should().BeFalse();
        }

        [Test]
        public void CausesInjury_WhenHasTrapAndLowProbability_ShouldBeFalse()
        {
            var randomMock = new Mock<RandomGenerator>();
            randomMock
                .Setup(mock => mock.GetProbabilityValue())
                .Returns(1);

            var trap = new MarshTrap(randomMock.Object);
            var room = new MarshRoom(1, false, new List<Trap>() {trap}, new Dictionary<Direction, int>());

            var trapActivated = room.CausesInjury();

            trapActivated.Should().BeFalse();
        }

        [Test]
        public void CausesInjury_WhenHasTrapAndHighProbability_ShouldBeFalse()
        {
            var randomMock = new Mock<RandomGenerator>();
            randomMock
                .Setup(mock => mock.GetProbabilityValue())
                .Returns(0);

            var trap = new MarshTrap(randomMock.Object);
            var room = new MarshRoom(1, false, new List<Trap>() { trap }, new Dictionary<Direction, int>());

            var trapActivated = room.CausesInjury();

            trapActivated.Should().BeTrue();
        }

        [Test]
        public void Description_WhenTrapActivated_ShouldBeUpdated()
        {
            var randomMock = new Mock<RandomGenerator>();
            randomMock
                .Setup(mock => mock.GetProbabilityValue())
                .Returns(0);

            var trap = new MarshTrap(randomMock.Object);
            var room = new MarshRoom(1, false, new List<Trap>() { trap }, new Dictionary<Direction, int>());

            var originalDescription = room.GetDescription();

            var trapActivated = room.CausesInjury();

            trapActivated.Should().BeTrue();

            var updatedDescription = room.GetDescription();

            updatedDescription.Should().NotBe(originalDescription);
        }
    }
}