using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace MazeIntegration.Tests
{
    [TestFixture]
    public class MazeIntegrationTests
    {
        private MazeIntegration _mazeIntegration;

        [SetUp]
        public void SetUp()
        {
            _mazeIntegration = new MazeIntegration();
        }

        [TestCase(3)]
        [TestCase(30)]
        public void FindTreasure_WhenNoTraps_ShouldFind(int mazeSize)
        {
            _mazeIntegration.BuildMaze(mazeSize);

            var entranceRoomId = _mazeIntegration.GetEntranceRoom();
            var markedRoomIds = new List<int>();

            var treasureRoomId = FindTreasureRoom(entranceRoomId, markedRoomIds);

            treasureRoomId.Should().NotBeNull();
        }

        private int? FindTreasureRoom(int roomId, List<int> markedRooms)
        {
            markedRooms.Add(roomId);

            if (_mazeIntegration.HasTreasure(roomId))
            {
                return roomId;
            }

            return CheckDirection(roomId, 'N', markedRooms)
                   ?? CheckDirection(roomId, 'E', markedRooms)
                   ?? CheckDirection(roomId, 'S', markedRooms)
                   ?? CheckDirection(roomId, 'W', markedRooms);
        }

        private int? CheckDirection(int roomId, char direction, List<int> markedRooms)
        {
            var nextRoomId = _mazeIntegration.GetRoom(roomId, direction);
            if (nextRoomId != null && !markedRooms.Contains(nextRoomId.Value))
            {
                return FindTreasureRoom(nextRoomId.Value, markedRooms);
            }

            return null;
        }
    }
}