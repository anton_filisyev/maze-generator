﻿using System;
using AtlasCopco.Integration.Maze;
using MazeGenerator.Factories;
using MazeGenerator.Mazes;
using MazeGenerator.Rooms;
using MazeGenerator.Types;
using MazeGenerator.Util;

namespace MazeIntegration
{
    public sealed class MazeIntegration: IMazeIntegration
    {
        private Maze _maze;

        public void BuildMaze(int size)
        {
            var random = new RandomGenerator();
            var mazeBuilderFactory = new MazeBuilderFactory(random);
            var builder = mazeBuilderFactory.GetMazeBuilder(MazeType.VerySimpleMaze);
            _maze = builder.Build(size);
        }

        public int GetEntranceRoom()
        {
            if (_maze == null)
            {
                throw new InvalidOperationException("Maze has not been instantiated yet");
            }

            return _maze.EntranceRoomId;
        }

        public int? GetRoom(int roomId, char directionCode)
        {
            if (_maze == null)
            {
                throw new InvalidOperationException("Maze has not been instantiated yet");
            }

            var direction = DirectionConverter.Convert(directionCode);
            var room = GetRoomById(roomId);
            return room.GetAdjacentRoomId(direction);
        }

        public string GetDescription(int roomId)
        {
            if (_maze == null)
            {
                throw new InvalidOperationException("Maze has not been instantiated yet");
            }

            var room = GetRoomById(roomId);
            return room.GetDescription();
        }

        public bool HasTreasure(int roomId)
        {
            if (_maze == null)
            {
                throw new InvalidOperationException("Maze has not been instantiated yet");
            }

            var room = GetRoomById(roomId);
            return room.HasTreasure;
        }

        public bool CausesInjury(int roomId)
        {
            if (_maze == null)
            {
                throw new InvalidOperationException("Maze has not been instantiated yet");
            }

            var room = GetRoomById(roomId);
            return room.CausesInjury();
        }

        private Room GetRoomById(int roomId)
        {
            var room = _maze.GetRoomById(roomId);
            if (room == null)
            {
                throw new InvalidOperationException($"Room with Id {roomId} was not found.");
            }

            return room;
        }
    }
}
