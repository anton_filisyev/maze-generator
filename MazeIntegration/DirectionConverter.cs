﻿using System;
using MazeGenerator.Types;

namespace MazeIntegration
{
    /// <summary>
    /// An utility class for converting direction codes into enum values
    /// </summary>
    static class DirectionConverter
    {
        /// <summary>
        /// Returns Direction value for direction code
        /// </summary>
        /// <param name="directionCode">A char representing direction code. Can be N, S, W or E.</param>
        /// <returns>Direction value</returns>
        public static Direction Convert(char directionCode)
        {
            switch (directionCode)
            {
                case 'N': return Direction.North;
                case 'W': return Direction.West;
                case 'S': return Direction.South;
                case 'E': return Direction.East;
                default:
                    throw new ArgumentException($"{directionCode} direction is not supported", nameof(directionCode));
            }
        }
    }
}
