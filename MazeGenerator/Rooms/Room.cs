﻿using System;
using System.Collections.Generic;
using MazeGenerator.Traps;
using MazeGenerator.Types;

namespace MazeGenerator.Rooms
{
    /// <summary>
    /// Base class for room entities
    /// </summary>
    public abstract class Room
    {
        protected List<Trap> Traps { get; }
        protected abstract string RoomDescription { get; }
        protected List<string> ActiveEffectDescriptions { get; }
        protected Dictionary<Direction, int> AdjacentRoomIds { get; }

        protected Room(int id, bool hasTreasure, List<Trap> traps, Dictionary<Direction, int> adjacentRooms)
        {
            Id = id;
            Traps = traps ?? new List<Trap>();
            AdjacentRoomIds = adjacentRooms ?? new Dictionary<Direction, int>();
            HasTreasure = hasTreasure;
            ActiveEffectDescriptions = new List<string>();
        }

        /// <summary>
        /// Room ID
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the computed textual description of the passed room.
        /// </summary>
        /// <returns>Textual room description.</returns>
        public virtual string GetDescription()
        {
            var activeEffectDescription = String.Join(", ", ActiveEffectDescriptions);
            return $"{RoomDescription}. {activeEffectDescription}";
        }

        /// <summary>
        /// Checks whether a room has a treasure.
        /// </summary>
        /// <returns>True if the room has a treasure, or false if not.</returns>
        public bool HasTreasure { get; }

        /// <summary>
        /// Checks whether a room causes injury to the player.
        /// </summary>
        /// <returns>True if the room causes injury (sinking, dehydration, etc), or false if not.</returns>
        public virtual bool CausesInjury()
        {
            foreach (var trap in Traps)
            {
                if (trap.CauseInjury())
                {
                    ActiveEffectDescriptions.Add(trap.BehaviorDescription);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the room adjacent to the passed room, given a direction. NULL signals invalid room, i.e. edge of the maze.
        /// </summary>
        /// <param name="direction">Direction to check</param>
        /// <returns>ID of the room relative to the originating room.</returns>
        public virtual int? GetAdjacentRoomId(Direction direction)
        {
            if (AdjacentRoomIds.ContainsKey(direction))
            {
                return AdjacentRoomIds[direction];
            }

            return null;
        }

        /// <summary>
        /// Gets map which indicates what traps each room type supports
        /// </summary>
        public static IReadOnlyDictionary<RoomType, IReadOnlyList<TrapType>> SupportedTrapTypes =>
            new Dictionary<RoomType, IReadOnlyList<TrapType>>()
            {
                {RoomType.Marsh, new List<TrapType>() {TrapType.Marsh}},
                {RoomType.Forest, new List<TrapType>()},
                {RoomType.Desert, new List<TrapType>() {TrapType.Desert}},
                {RoomType.Hills, new List<TrapType>()}
            };
    }
}