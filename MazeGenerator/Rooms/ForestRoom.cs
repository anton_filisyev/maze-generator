﻿using System.Collections.Generic;
using MazeGenerator.Traps;
using MazeGenerator.Types;

namespace MazeGenerator.Rooms
{
    public class ForestRoom : Room
    {
        public ForestRoom(int id, bool hasTreasure, List<Trap> traps, Dictionary<Direction, int> adjacentRooms)
            : base(id, hasTreasure, traps, adjacentRooms)
        {
        }

        protected override string RoomDescription => "Forest with rich vegetation";
    }
}