﻿using System.Collections.Generic;
using MazeGenerator.Traps;
using MazeGenerator.Types;

namespace MazeGenerator.Rooms
{
    public class HillsRoom : Room
    {
        public HillsRoom(int id, bool hasTreasure, List<Trap> traps, Dictionary<Direction, int> adjacentRooms)
            : base(id, hasTreasure, traps, adjacentRooms)
        {
        }

        protected override string RoomDescription => "Green hills blown by the north wind";
    }
}