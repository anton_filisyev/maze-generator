﻿using System.Collections.Generic;
using MazeGenerator.Rooms;

namespace MazeGenerator.Mazes
{
    /// <summary>
    /// Very simple maze
    /// </summary>
    public class VerySimpleMaze : Maze
    {
        public static int MinSize => 2;

        public VerySimpleMaze(List<Room> rooms, int entranceRoomId) 
            : base(rooms, entranceRoomId)
        {
        }
    }
}