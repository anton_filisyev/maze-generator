﻿using System.Collections.Generic;
using System.Linq;
using MazeGenerator.Rooms;

namespace MazeGenerator.Mazes
{
    /// <summary>
    /// Base class for maze entities
    /// </summary>
    public abstract class Maze
    {
        protected readonly List<Room> _rooms;
        public IReadOnlyList<Room> Rooms => _rooms.AsReadOnly();

        /// <summary>
        /// Entrance room ID
        /// </summary>
        public int EntranceRoomId { get; }

        protected Maze(List<Room> rooms, int entranceRoomId)
        {
            _rooms = rooms;
            EntranceRoomId = entranceRoomId;
        }

        /// <summary>
        /// Returns room by its ID
        /// </summary>
        /// <param name="roomId">Room ID to search</param>
        /// <returns>Room with the corresponding ID or NULL</returns>
        public Room GetRoomById(int roomId)
        {
            var room = _rooms.SingleOrDefault(x => x.Id == roomId);
            return room;
        }
    }
}