﻿namespace MazeGenerator.Types
{
    public enum RoomType
    {
        Forest = 1,
        Marsh = 2,
        Desert = 3,
        Hills = 4
    }
}