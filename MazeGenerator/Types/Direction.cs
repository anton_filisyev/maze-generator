﻿namespace MazeGenerator.Types
{
    public enum Direction
    {
        North = 1,
        West = 2,
        South = 3,
        East = 4
    }
}
