﻿using System.Collections.Generic;
using MazeGenerator.Traps;
using MazeGenerator.Types;

namespace MazeGenerator
{
    public sealed class RoomFrame
    {
        public int Id { get; set; }

        public RoomType RoomType { get; set; }

        public bool IsEntrance { get; set; }

        public bool HasTreasure { get; set; }

        public List<Trap> Traps { get; set; }

        public Dictionary<Direction, int> AdjacentRoomIds { get; set; }

        public RoomFrame()
        {
            Traps = new List<Trap>();
            AdjacentRoomIds = new Dictionary<Direction, int>();
        }
    }
}