﻿using System;
using MazeGenerator.Mazes;
using MazeGenerator.Types;
using MazeGenerator.Util;

namespace MazeGenerator.MazeBuilders
{
    /// <summary>
    /// Base class for maze builders
    /// </summary>
    public abstract class MazeBuilder<TMaze> : IMazeBuilder<TMaze>
        where TMaze: Maze
    {
        protected readonly RandomGenerator Random;

        /// <summary>
        /// The minimal size for a particular maze type
        /// </summary>
        protected abstract int MinMazeSize { get; }

        protected MazeFrame Frame { get; private set; }

        protected MazeBuilder(RandomGenerator random)
        {
            Random = random;
        }

        /// <summary>
        /// Builds a new maze
        /// </summary>
        public TMaze Build(int size)
        {
            if (size < MinMazeSize)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }

            Frame = new MazeFrame(size);

            CreateRoomFrames();

            SetEntranceRoom();

            SetTreasureRoom();

            SetAdjacentRoom();

            InitRooms();

            var maze = CreateMaze();
            return maze;
        }

        /// <summary>
        /// Creates initial rooms
        /// </summary>
        protected virtual void CreateRoomFrames()
        {
            var roomId = 1;

            for (int i = 0; i < Frame.Size; i++)
            {
                for (int j = 0; j < Frame.Size; j++)
                {
                    var roomType = Random.GetEnumValue<RoomType>();
                    var roomFrame = new RoomFrame()
                    {
                        Id = roomId,
                        RoomType = roomType
                    };
                    Frame.SetRoom(i, j, roomFrame);
                    roomId++;
                }
            }
        }

        /// <summary>
        /// Initializes rooms properties
        /// </summary>
        protected abstract void InitRooms();

        /// <summary>
        /// Creates Maze based on initialized rooms
        /// </summary>
        protected abstract TMaze CreateMaze();

        /// <summary>
        /// Sets entrance room
        /// </summary>
        protected abstract void SetEntranceRoom();

        /// <summary>
        /// Sets room with treasure
        /// </summary>
        protected abstract void SetTreasureRoom();

        /// <summary>
        /// Sets adjacent rooms
        /// </summary>
        protected virtual void SetAdjacentRoom()
        {
            for (int i = 0; i < Frame.Size; i++)
            {
                for (int j = 0; j < Frame.Size; j++)
                {
                    var room = Frame.GetRoom(i, j);
                    if (room == null)
                    {
                        continue;
                    }

                    //Set North
                    if (j + 1 < Frame.Size)
                    {
                        var northRoom = Frame.GetRoom(i, j + 1);
                        if (northRoom != null)
                        {
                            room.AdjacentRoomIds.Add(Direction.North, northRoom.Id);
                        }
                    }

                    //Set South
                    if (j > 0)
                    {
                        var southRoom = Frame.GetRoom(i, j - 1);
                        if (southRoom != null)
                        {
                            room.AdjacentRoomIds.Add(Direction.South, southRoom.Id);
                        }
                    }

                    //Set East
                    if (i + 1 < Frame.Size)
                    {
                        var eastRoom = Frame.GetRoom(i + 1, j);
                        if (eastRoom != null)
                        {
                            room.AdjacentRoomIds.Add(Direction.East, eastRoom.Id);
                        }
                    }

                    //Set West
                    if (i > 0)
                    {
                        var westRoom = Frame.GetRoom(i - 1, j);
                        if (westRoom != null)
                        {
                            room.AdjacentRoomIds.Add(Direction.West, westRoom.Id);
                        }
                    }
                }
            }
        }
    }
}