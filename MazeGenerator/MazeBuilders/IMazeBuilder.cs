﻿using MazeGenerator.Mazes;

namespace MazeGenerator.MazeBuilders
{
    public interface IMazeBuilder<out TMaze>
        where TMaze : Maze
    {
        TMaze Build(int size);
    }
}