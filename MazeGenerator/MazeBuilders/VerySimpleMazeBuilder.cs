﻿using System.Collections.Generic;
using System.Linq;
using MazeGenerator.Factories;
using MazeGenerator.Mazes;
using MazeGenerator.Rooms;
using MazeGenerator.Util;

namespace MazeGenerator.MazeBuilders
{
    public sealed class VerySimpleMazeBuilder: MazeBuilder<VerySimpleMaze>
    {
        private readonly RoomFactory _roomFactory;
        private readonly TrapFactory _trapFactory;
        private const double ChanceHavingTrap = 0.5;

        public VerySimpleMazeBuilder(RoomFactory roomFactory, TrapFactory trapFactory, RandomGenerator random)
            : base(random)
        {
            _roomFactory = roomFactory;
            _trapFactory = trapFactory;
        }

        protected override int MinMazeSize => VerySimpleMaze.MinSize;
        
        protected override void InitRooms()
        {
            var rooms = Frame.GetEmptyRooms();

            foreach (var room in rooms)
            {
                SetTraps(room);
            }
        }

        protected override VerySimpleMaze CreateMaze()
        {
            var roomFrames = Frame.GetRooms();

            var rooms = new List<Room>();
            foreach (var roomFrame in roomFrames)
            {
                var room = _roomFactory.CreateRoom(roomFrame.RoomType, roomFrame.Id, roomFrame.HasTreasure, roomFrame.Traps, roomFrame.AdjacentRoomIds);
                rooms.Add(room);
            }

            int entranceRoomId = roomFrames.Single(x => x.IsEntrance).Id;
            var maze = new VerySimpleMaze(rooms, entranceRoomId);
            return maze;
        }

        protected override void SetEntranceRoom()
        {
            var borderlineRooms = Frame.GetBorderlineRooms();

            var room = GetRandomRoom(borderlineRooms);
            room.IsEntrance = true;
        }

        protected override void SetTreasureRoom()
        {
            var rooms = Frame.GetEmptyRooms();

            var room = GetRandomRoom(rooms);

            room.HasTreasure = true;
        }

        private RoomFrame GetRandomRoom(List<RoomFrame> rooms)
        {
            var index = Random.GetNumber(0, rooms.Count);
            return rooms[index];
        }

        private void SetTraps(RoomFrame room)
        {
            var supportedTrapTypes = Room.SupportedTrapTypes[room.RoomType];

            foreach (var supportedTrapType in supportedTrapTypes)
            {
                var setTrap = NeedSetTrap();
                if (setTrap)
                {
                    var trap = _trapFactory.CreateTrap(supportedTrapType);
                    room.Traps.Add(trap);
                }
            }
        }

        private bool NeedSetTrap()
        {
            return Random.GetProbabilityValue() < ChanceHavingTrap;
        }
    }
}
