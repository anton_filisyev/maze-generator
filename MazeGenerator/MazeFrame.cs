﻿using System;
using System.Collections.Generic;

namespace MazeGenerator
{
    public sealed class MazeFrame
    {
        public int Size { get; }

        private RoomFrame[,] Frame { get; }

        public MazeFrame(int size)
        {
            if (size < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }

            Size = size;
            Frame = new RoomFrame[Size, Size];
        }

        public RoomFrame GetRoom(int row, int column)
        {
            if (row >= Size)
            {
                throw new ArgumentOutOfRangeException(nameof(row));
            }
            if (column >= Size)
            {
                throw new ArgumentOutOfRangeException(nameof(column));
            }

            return Frame[row, column];
        }

        public void SetRoom(int row, int column, RoomFrame room)
        {
            if (row >= Size)
            {
                throw new ArgumentOutOfRangeException(nameof(row));
            }
            if (column >= Size)
            {
                throw new ArgumentOutOfRangeException(nameof(column));
            }

            Frame[row, column] = room;
        }

        public List<RoomFrame> GetRooms()
        {
            var allRooms = new List<RoomFrame>(Frame.Length);

            foreach (var room in Frame)
            {
                allRooms.Add(room);
            }

            return allRooms;
        }

        public List<RoomFrame> GetEmptyRooms()
        {
            var allRooms = new List<RoomFrame>(Frame.Length);

            foreach (var room in Frame)
            {
                if (room.IsEntrance || room.HasTreasure)
                {
                    continue;
                }

                allRooms.Add(room);
            }

            return allRooms;
        }

        public List<RoomFrame> GetBorderlineRooms()
        {
            var rooms = new List<RoomFrame>();

            if (Size == 1)
            {
                rooms.Add(Frame[0, 0]);
                return rooms;
            }

            var firstRowInd = 0;
            var lastRowInd = Size - 1;
            for (int j = 0; j < Size; j++)
            {
                rooms.Add(Frame[firstRowInd, j]);
                rooms.Add(Frame[lastRowInd, j]);
            }

            if (Size > 2)
            {
                var firstColInd = 1;
                var lastColInd = Size - 2;
                for (int i = 0; i < Size; i++)
                {
                    rooms.Add(Frame[i, firstColInd]);
                    rooms.Add(Frame[i, lastColInd]);
                }
            }

            return rooms;
        }
    }
}