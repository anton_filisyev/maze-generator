﻿using System;

namespace MazeGenerator.Util
{
    /// <summary>
    /// An utility class for generating random values
    /// </summary>
    public class RandomGenerator
    {
        private static readonly Random _random = new Random(DateTime.UtcNow.Millisecond);

        /// <summary>
        /// Returns a random floating-point number that is greater than or equal to 0.0, and less than 1.0. 
        /// </summary>
        /// <returns>A double-precision floating point number that is greater than or equal to 0.0, and less than 1.0.</returns>
        public virtual double GetProbabilityValue()
        {
            return _random.NextDouble();
        }

        /// <summary>
        /// Returns a random integer that is within a specified range.
        /// </summary>
        /// <param name="minValue">The inclusive lower bound of the random number returned.</param>
        /// <param name="maxValue">The exclusive upper bound of the random number returned. maxValue must be greater than or equal to minValue.</param>
        /// <returns>A 32-bit signed integer greater than or equal to minValue and less than maxValue.</returns>
        public virtual int GetNumber(int minValue, int maxValue)
        {
            return _random.Next(minValue, maxValue);
        }

        /// <summary>
        /// Returns a random enum value.
        /// </summary>
        /// <typeparam name="TEnum">Enum type</typeparam>
        /// <returns>A enum value</returns>
        public virtual TEnum GetEnumValue<TEnum>()
            where TEnum : Enum
        {
            var values = Enum.GetValues(typeof(TEnum));
            return (TEnum)values.GetValue(_random.Next(values.Length));
        }
    }
}
