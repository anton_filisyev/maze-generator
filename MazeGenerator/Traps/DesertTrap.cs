﻿using MazeGenerator.Util;

namespace MazeGenerator.Traps
{
    public class DesertTrap : Trap
    {
        public DesertTrap(RandomGenerator random) : base(random)
        {
        }

        protected override double ChanceToCause => 0.2;

        public override string BehaviorDescription => "Desert that cannot be overcome without water";
    }
}