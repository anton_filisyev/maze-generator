﻿using MazeGenerator.Util;

namespace MazeGenerator.Traps
{
    public class MarshTrap : Trap
    {
        public MarshTrap(RandomGenerator random) : base(random)
        {
        }

        protected override double ChanceToCause => 0.3;

        public override string BehaviorDescription => "Swamp you can't get out of";
    }
}