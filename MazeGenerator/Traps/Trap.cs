﻿using MazeGenerator.Util;

namespace MazeGenerator.Traps
{
    /// <summary>
    /// Base class for trap entities
    /// </summary>
    public abstract class Trap
    {
        protected readonly RandomGenerator _random;
        protected abstract double ChanceToCause { get; }

        protected Trap(RandomGenerator random)
        {
            _random = random;
        }
        
        public abstract string BehaviorDescription { get; }

        public virtual bool CauseInjury()
        {
            var rnd = _random.GetProbabilityValue();
            return rnd < ChanceToCause;
        }
    }
}
