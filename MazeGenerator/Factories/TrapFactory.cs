﻿using System;
using MazeGenerator.Traps;
using MazeGenerator.Types;
using MazeGenerator.Util;

namespace MazeGenerator.Factories
{
    /// <summary>
    /// Factory class for creating trap entities
    /// </summary>
    public class TrapFactory
    {
        private readonly RandomGenerator _random;

        public TrapFactory(RandomGenerator random)
        {
            _random = random;
        }

        /// <summary>
        /// Returns a trap of the corresponding type
        /// </summary>
        /// <param name="type">Trap type to instantiate</param>
        /// <returns>A trap entity</returns>
        public virtual Trap CreateTrap(TrapType type)
        {
            switch (type)
            {
                case TrapType.Marsh:
                    return new MarshTrap(_random);
                case TrapType.Desert:
                    return new DesertTrap(_random);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, "Trap type is not supported.");
            }
        }
    }
}