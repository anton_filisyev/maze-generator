﻿using System;
using System.Collections.Generic;
using MazeGenerator.Rooms;
using MazeGenerator.Traps;
using MazeGenerator.Types;

namespace MazeGenerator.Factories
{
    /// <summary>
    /// Factory class for creating room entities
    /// </summary>
    public class RoomFactory
    {
        /// <summary>
        /// Returns a room entity of the corresponding type.
        /// </summary>
        /// <param name="type">Room type to instantiate</param>
        /// <param name="id">Room ID</param>
        /// <param name="hasTreasure">Indicates whether room contains a treasure</param>
        /// <param name="traps">Traps which room contains</param>
        /// <param name="adjacentRooms">Configuration of adjacent rooms</param>
        /// <returns>A room entity</returns>
        public virtual Room CreateRoom(RoomType type, int id, bool hasTreasure, List<Trap> traps, Dictionary<Direction, int> adjacentRooms)
        {
            switch (type)
            {
                case RoomType.Forest: 
                    return new ForestRoom(id, hasTreasure, traps, adjacentRooms);
                case RoomType.Marsh: 
                    return new MarshRoom(id, hasTreasure, traps, adjacentRooms);
                case RoomType.Desert: 
                    return new DesertRoom(id, hasTreasure, traps, adjacentRooms);
                case RoomType.Hills: 
                    return new HillsRoom(id, hasTreasure, traps, adjacentRooms);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, "Room type is not supported.");
            }
        }
    }
}
