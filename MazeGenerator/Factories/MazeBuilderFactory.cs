﻿using System;
using MazeGenerator.MazeBuilders;
using MazeGenerator.Mazes;
using MazeGenerator.Types;
using MazeGenerator.Util;

namespace MazeGenerator.Factories
{
    /// <summary>
    /// Factory class for creating maze builders
    /// </summary>
    public class MazeBuilderFactory
    {
        private readonly RandomGenerator _random;
        public MazeBuilderFactory(RandomGenerator random)
        {
            _random = random;
        }

        /// <summary>
        /// Returns a maze builder of the corresponding type
        /// </summary>
        /// <param name="mazeType">Maze type to instantiate</param>
        /// <returns>A maze builder</returns>
        public virtual IMazeBuilder<Maze> GetMazeBuilder(MazeType mazeType)
        {
            switch (mazeType)
            {
                case MazeType.VerySimpleMaze:
                    return new VerySimpleMazeBuilder(new RoomFactory(), new TrapFactory(_random), _random);
                default:
                    throw new ArgumentOutOfRangeException(nameof(mazeType), mazeType, "Maze type is not supported.");
            }
        }
    }
}